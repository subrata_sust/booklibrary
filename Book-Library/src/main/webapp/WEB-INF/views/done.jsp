<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form:form commandName="doneCmd" enctype="multipart/form-data">
    <div class="container">
        <div class="col-md-6 pull-left">
            <c:if test="${not empty doneCmd.messageKey }">
                <fmt:message code="${doneCmd.messageKey}"/>
            </c:if>
        </div>
    </div>
</form:form>
</body>
</html>
