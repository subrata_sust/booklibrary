<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Shuvo
  Date: 12/1/2017
  Time: 9:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload Excel</title>
</head>
<body>
<form:form commandName="excelInfoCmd" enctype="multipart/form-data" method="post">
    <div class="container">\
        <div class="col-md-6 pull-left">

            <fmt:message code="label.excel.upload"/>
            <form:input path="file" name="file" type="file"/>

            <fmt:message code="label.name"/>
            <form:input path="name" name="name" type="text"/>

            <input type="submit" value="Upload">
        </div>
    </div>
</form:form>

</body>
</html>
