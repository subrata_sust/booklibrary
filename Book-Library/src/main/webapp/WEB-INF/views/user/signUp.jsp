<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Shuvo
  Date: 12/1/2017
  Time: 9:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload Excel</title>
</head>
<body>

<div class="container">
    <form:form modelAttribute="login" method="post">
        <div class="row">
            <div class="col-md-6">
                <div class="login-area shadow-depth-1">
                    <fieldset>
                        <!-- Form Name -->
                        <legend><fmt:message code="legend.signUp"/></legend>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">
                                <fmt:message code="label.email"/>
                            </label>

                            <div class="col-md-8">
                                <form:input path="email" id="email" name="email" type="email" placeholder="your email"
                                            class="form-control input-md" required="required"/>
                                <form:errors path="email" class="text-danger"/>
                            </div>

                            <label class="col-md-4 control-label" for="name">
                                <fmt:message code="label.name"/>
                            </label>

                            <div class="col-md-8">
                                <form:input path="name" id="name" name="name" type="text" placeholder="your name"
                                            class="form-control input-md" required="required"/>
                                <form:errors path="name" class="text-danger"/>
                            </div>

                            <label class="col-md-4 control-label" for="password">
                                <fmt:message code="label.password"/>
                            </label>

                            <div class="col-md-8">
                                <form:input path="password" id="password" name="password" type="password"
                                            placeholder="your password"
                                            class="form-control input-md" required="required"/>
                                <form:errors path="password" class="text-danger"/>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class=" form-group">
                            <label class="col-md-4 control-label" for="login"></label>

                            <div class="col-md-8">
                                <button id="login" name="" class="btn btn-info">
                                    <fmt:message code="button.submit"/>
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </form:form>

</div>
</body>
</html>

