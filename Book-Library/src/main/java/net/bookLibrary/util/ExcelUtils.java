package net.bookLibrary.util;

import net.bookLibrary.web.command.excel.ExcelInfoCmd;
import net.bookLibrary.web.command.excel.ExcelPersonCmd;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelUtils {

    public static List<ExcelPersonCmd> parseExcel(ExcelInfoCmd excelInfoCmd) throws IOException {
        List<ExcelPersonCmd> excelPersonCmds = new ArrayList<>();

        InputStream in = excelInfoCmd.getFile().getInputStream();

        Workbook workbook = new HSSFWorkbook(in);

        Sheet sheet = workbook.getSheetAt(0);
        HSSFRow row;
        HSSFCell cell;

        Iterator rows = sheet.iterator();

        rows.next();

        while (rows.hasNext()) {
            row = (HSSFRow) rows.next();

            ExcelPersonCmd excelPersonCmd = new ExcelPersonCmd();

            excelPersonCmd.setRegNo(getLongValue(0, row));
            excelPersonCmd.setName(getStringValue(1, row));
            excelPersonCmd.setAddress(getStringValue(2, row));
            excelPersonCmd.setPassword(getStringValue(3, row));
            excelPersonCmd.setMobileNo(getStringValue(4, row));

            excelPersonCmds.add(excelPersonCmd);
        }

        return excelPersonCmds;
    }

    private static String getStringValue(int index, HSSFRow row) {
        if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_STRING)
            return row.getCell(index).getStringCellValue();
        else if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
            return "" + row.getCell(index).getNumericCellValue();

        return "";
    }

    private static long getLongValue(int index, HSSFRow row) {
        if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_STRING)
            return Integer.parseInt(row.getCell(index).getStringCellValue());
        else if (row.getCell(index).getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
            return (long) row.getCell(index).getNumericCellValue();

        return 0;
    }
}
