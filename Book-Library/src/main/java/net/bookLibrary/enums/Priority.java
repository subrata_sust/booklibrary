package net.bookLibrary.enums;

public enum Priority {
    HIGH(0),
    MEDIUM(1),
    LOW(2);

    private final int notiLevel;

    Priority(int i) {
        notiLevel = i;
    }

    public int getNotiLevel(){
        return notiLevel;
    }
}