package net.bookLibrary.service;

import net.bookLibrary.domain.Login;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class LoginService {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void save(Login login) {
        entityManager.persist(login);
    }

    @Transactional
    public boolean notExists(String email) {
        return entityManager.createQuery("From Login Where email = :email")
                .setParameter("email", email)
                .getResultList()
                .size() == 0;
    }

    public Login get(long id) {
        return entityManager.find(Login.class, id);
    }
}
