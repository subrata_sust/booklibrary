package net.bookLibrary.web.jms;

import net.bookLibrary.domain.EventData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageReceiver {

    private static final Logger LOG = LoggerFactory.getLogger(MessageReceiver.class);

    @JmsListener(destination = "MyQueue")
    public void receiveMessage(EventData eventData) {
        LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        LOG.info("Application : headers received : {}", eventData.getMessage());
    }
}