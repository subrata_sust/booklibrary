package net.bookLibrary.web.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import net.bookLibrary.domain.EventData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(final EventData eventData) {

        jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage(eventData);
            }
        });
    }

}