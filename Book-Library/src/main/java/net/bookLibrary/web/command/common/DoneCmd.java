package net.bookLibrary.web.command.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DoneCmd implements Serializable{
    private static final long serialVersionUID = 1L;

    private String messageKey;
    private String backLink;
    private String formUrl;
    private String nextUrl;
    private Map<String, String> actionMap = new HashMap();

    public DoneCmd(){

    }

    public DoneCmd(String messageKey, String backLink, String formUrl, String nextUrl, Map<String, String> actionMap) {
        this.messageKey = messageKey;
        this.backLink = backLink;
        this.formUrl = formUrl;
        this.nextUrl = nextUrl;
        this.actionMap = actionMap;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getBackLink() {
        return backLink;
    }

    public void setBackLink(String backLink) {
        this.backLink = backLink;
    }

    public String getFormUrl() {
        return formUrl;
    }

    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public Map<String, String> getActionMap() {
        return actionMap;
    }

    public void setActionMap(Map<String, String> actionMap) {
        this.actionMap = actionMap;
    }
}
