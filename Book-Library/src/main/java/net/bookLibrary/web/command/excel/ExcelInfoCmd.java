package net.bookLibrary.web.command.excel;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class ExcelInfoCmd implements Serializable{
    private static final long serialVersionUID = 1L;

    private MultipartFile file;

    private String name;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
