package net.bookLibrary.web.RestController;

import net.bookLibrary.domain.Login;
import net.bookLibrary.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping(value = "/api/user")
public class MobileLoginController {

    @Autowired
    LoginService loginService;

    @GetMapping("/{id}")
    public Login showSignUpForm(@PathVariable("id") long id) {

        Login login = loginService.get(id);

//        login.add(linkTo(MobileLoginController.class).slash(login.getId()).withSelfRel());

        return login;
    }
}
