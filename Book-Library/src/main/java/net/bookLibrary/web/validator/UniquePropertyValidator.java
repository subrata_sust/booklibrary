package net.bookLibrary.web.validator;

import net.bookLibrary.annotation.Unique;
import net.bookLibrary.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniquePropertyValidator implements ConstraintValidator<Unique, String> {

    @Autowired
    private LoginService loginService;

    private String propertyName;

    @Override
    public void initialize(Unique constraintAnnotation) {
        propertyName = constraintAnnotation.fieldName();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        switch (propertyName) {
            case "email":
                return loginService.notExists(value);
            default:
                return false;
        }
    }
}
