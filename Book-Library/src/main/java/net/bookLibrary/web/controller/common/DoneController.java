package net.bookLibrary.web.controller.common;

import net.bookLibrary.web.command.common.DoneCmd;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/done")
public class DoneController {

    private static final String COMMAND = "doneCmd";

    @GetMapping
    public String view(@ModelAttribute(COMMAND) DoneCmd doneCmd, ModelMap model) {

        if (doneCmd == null) {
            return "redirect: /uploadExcel";
        }

        model.put(COMMAND, doneCmd);

        return "done";
    }

}
