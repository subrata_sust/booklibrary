package net.bookLibrary.web.controller.user;

import net.bookLibrary.domain.EventData;
import net.bookLibrary.domain.Login;
import net.bookLibrary.enums.Priority;
import net.bookLibrary.service.LoginService;
import net.bookLibrary.web.jms.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.validation.Valid;

@Controller
@SessionAttributes(LoginController.COMMAND_NAME)
public class LoginController {

    protected static final String COMMAND_NAME = "login";

    @Autowired
    LoginService loginService;

    @Autowired
    private MessageSender messageSender;

    @GetMapping("/signUp")
    public String showSignUpForm(ModelMap model) {

        model.put(COMMAND_NAME, new Login());
        messageSender.sendMessage(new EventData(1,"ALL Thanks to GOD..Forgive me my lord for our fault.", Priority.HIGH));

        return "/user/signUp";
    }

    @PostMapping("/signUp")
    public String submitSignUpForm(@Valid @ModelAttribute(COMMAND_NAME) Login login, BindingResult result) {

        if (result.hasErrors()) {
            return "/user/signUp";
        }

        loginService.save(login);

        return "redirect:" + "/home";
    }
}
