package net.bookLibrary.web.controller.user;

import net.bookLibrary.util.ExcelUtils;
import net.bookLibrary.web.command.excel.ExcelInfoCmd;
import net.bookLibrary.web.command.common.DoneCmd;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@Controller
@RequestMapping("/uploadExcel")
public class UploadExcelController {

    public static final String COMMAND = "excelInfoCmd";

    @GetMapping
    public String showForm(ModelMap model) {

        model.put(COMMAND, new ExcelInfoCmd());

        return "/uploadExcel";
    }

    @PostMapping
    public String showForm(@ModelAttribute ExcelInfoCmd excelInfoCmd, RedirectAttributes redirectAttributes) throws IOException {
        if (excelInfoCmd.getFile() == null) {
            return "/uploadExcel";
        }

        ExcelUtils.parseExcel(excelInfoCmd);

        redirectAttributes.addFlashAttribute("doneCmd",
                new DoneCmd("label.excel.upload.succcess", null, null, null, null));

        return "redirect:" + "/done";
    }
}
