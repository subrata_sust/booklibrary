package net.bookLibrary.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Logger {

    @Around("@annotation(net.bookLibrary.annotation.Log)")
    public void log(ProceedingJoinPoint proceedingJoinPoint) throws Exception {
        System.out.println("Before handler method");
        try {
            proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        System.out.println("After handler method");
    }
}
