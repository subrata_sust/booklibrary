package net.bookLibrary.domain;

import net.bookLibrary.enums.Priority;

import java.io.Serializable;

public class EventData implements Serializable{

    private static final long serialVersionUID = 1L;

    private int id;

    private String message;

    private Priority priority;

    public EventData(int id, String message, Priority priority) {
        this.id = id;
        this.message = message;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }
}
